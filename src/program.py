def get_sum(N):
    if N <= 0:
        return 0
    s = 0
    for i in range(N):
        s += i
    return s

if __name__ == "__main__":
    print('Programm that sum number from 1 to N')
    N = int(input("Enter N: "))
    print(f'Summ is {get_sum(N)}')


