import os, sys, inspect
# получаем директорию файла
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
# получаем родительскую директорию
parentdir = os.path.dirname(currentdir)
# получаем директорую скриптов
src_path = os.path.join(parentdir, 'src')
# добавляем директорию к путям
sys.path.insert(0, src_path) 

from program import get_sum

def test_zero_sum():
    assert get_sum(0) == 0

def test_positive_sum():
    assert get_sum(10) == 45

def test_negative_sum():
    assert get_sum(-5) == 0
    

